import unittest

import requests
import json

# from schemas import validate_dsi_predictions

class TestApi(unittest.TestCase):
    def test_api_single(self):
        # Here we invoke the algorithm, using the media file we provided
        response = requests.post(
            "http://localhost:5000/v1/analyse",
            files={"image": open("data/10971819.jpg", "rb")},
        )
        self.assertEqual(200, response.status_code)
        json_response = response.json()
        print(json.dumps(json_response, indent=2))
        json.dump(json_response, open("api_response.json", "w"), indent=2)

        # Here we use the official validation script
        # validator_result = validate_dsi_predictions.validate_json_predictions_file("api_response.json")
        # self.assertEqual(0, validator_result)

        # Here we can add a couple of checks of our own, if desired
        # These extra checks will depend on any extra logic you have in your algorithm output
        num_detections = 1
        self.assertEqual(num_detections, len(json_response["region_groups"]))
        self.assertEqual(num_detections, len(json_response["predictions"]))
        self.assertEqual(1, len(json_response["media"]))

        print(f"Algorithm output covers {len(json_response['media'])} media file(s),  {len(json_response['region_groups'])} region group(s), and  {len(json_response['predictions'])} prediction set(s)")

tt = TestApi()
tt.test_api_single()