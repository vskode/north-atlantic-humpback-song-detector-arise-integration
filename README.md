This repository contains a template that you can use as a starting point to implement your own nature recognition algorithm, in a format that can be used directly with Arise DSI.

To build the Docker container:

    docker build . -t arise-detection

To start the container:

    docker run -p 5000:5000 arise-detection

To get ready for testing your algorithm's output, install the environment:

    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.test.txt
    git clone "https://gitlab.com/arise-biodiversity/schemas.git" tests/schemas

To test your API, including JSON schema checking, make sure the Docker container has been started (as above) and then run:

    source venv/bin/activate
    pytest --capture=no

