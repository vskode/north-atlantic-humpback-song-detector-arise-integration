FROM python:3.8.2-slim-buster

MAINTAINER Laurens Hogeweg "laurens.hogeweg@naturalis.com"

RUN apt update
RUN apt install -y --allow-unauthenticated screen nano rsync
RUN apt install -y --allow-unauthenticated git

RUN python -m pip install --no-cache-dir -U pip
RUN python -m pip install --no-cache-dir -U setuptools

COPY requirements.txt /tmp/
RUN pip install --requirement /tmp/requirements.txt

COPY . /usr/src/app/
WORKDIR /usr/src/app

ENV PYTHON_UNBUFFERED 1

CMD ["python", "src/arise_server.py"]