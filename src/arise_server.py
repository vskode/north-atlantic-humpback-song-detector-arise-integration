import datetime
import logging
from collections import namedtuple

import numpy as np
import os
import tempfile
import flask
from typing import Dict, Any, List


def str2bool(value, raise_exc=False) -> bool:
    """
    Converts a string to boolean by matching it with some well known representations of bool.
    {"yes", "true", "t", "y", "1"} --> true
    {"no", "false", "f", "n", "0"} --> false
    If value is not one of these representations it returns False or raises a ValueError when raise_exc==True
    :param value: a string representing a boolean
    :param raise_exc: if True and unknown boolean string representation raises a ValueError
    :raises ValueError
    :return: a boolean
    """
    _true_set = {"yes", "true", "t", "y", "1"}
    _false_set = {"no", "false", "f", "n", "0"}
    result = False
    value_ = str(value).lower()
    if value_ in _true_set:
        result = True
    elif value_ in _false_set:
        result = False
    else:
        if raise_exc:
            raise ValueError(
                'Expected "{}"'.format('", "'.join(_true_set | _false_set))
            )
    return result


def get_logger(name: str, filename: str) -> logging.Logger:
    """
    Configures a logger with name 'name'. If the name is empty ('') then you can just use logging.info(..),
    else use logging.getLogger(name).info() or the returned logger object.
    Needs environment variable LOG_FOLDER to be set
    :param name:
    :param filename:
    :return: logging.Logger
    """
    log_folder = os.environ.get("LOG_FOLDER", os.getcwd())

    if not os.path.exists(log_folder):
        os.makedirs(log_folder)
    filename = os.path.join(log_folder, filename)
    logger_ = logging.getLogger(name)
    logger_.setLevel(logging.INFO)
    logger_.handlers = []
    formatter = logging.Formatter("%(asctime)s [%(name)s] %(message)s")
    file_logger = logging.FileHandler(filename)
    file_logger.setFormatter(formatter)
    file_logger.setLevel(logging.INFO)
    logger_.addHandler(file_logger)
    console = logging.StreamHandler()
    console.setFormatter(formatter)
    console.setLevel(logging.DEBUG)
    logger_.addHandler(console)

    return logger_


class ApiException(BaseException):
    """
    Base class for raising and catching API specific exceptions
    """

    pass


app = flask.Flask(__name__, static_url_path="", static_folder="static")
logger = get_logger("ARISE_GETI_MODEL_SERVER", os.getenv("LOG_PATH", __file__ + ".log"))
JsonType = Dict[str, Any]


@app.route("/v1/analyse", methods=["POST"])
def analyse():
    """
    Analyses an image

    Expects POST parameter 'image'
    """
    # get uploaded photos
    uploaded_files = flask.request.files.getlist("image")
    logger.info("Number of uploaded files: {}".format(len(uploaded_files)))
    if len(uploaded_files) < 1:
        raise ApiException("No images uploaded")
    #
    out_json = {
        "name": "https://schemas.arise-biodiversity.nl/dsi/multi-object-multi-image#sequence-one-prediction-per-region",
        # TODO: there is no public URL with the schema yet
        "generated_by": {
            "datetime": datetime.datetime.now().isoformat() + "Z",
            "version": os.getenv("ALGORITHM_VERSION", "algorithm version unknown"),
            "tag": os.getenv("ALGORITHM_TAG", "algorithm tag unknown"),
        },
        "media": [],
        "region_groups": [],
        "predictions": [],
    }

    # -- for uploaded image perform inference
    uploaded_file = uploaded_files[0]  # for now assume one uploaded file
    uploaded_name = uploaded_file.filename

    ClassifiedBox = namedtuple(
        "ClassifiedBox", ["x1", "y1", "x2", "y2", "probability", "name"]
    )

    with tempfile.TemporaryDirectory() as tmp_dir:
        # write the uploaded file to temp dir
        image_path = os.path.join(tmp_dir, uploaded_name)
        with open(image_path, "wb") as f:
            f.write(uploaded_file.read())

        # TODO: perform model inference

        # TODO: add boxes/classifications from inference here
        boxes: List[ClassifiedBox] = []

        # now a hardcoded example
        boxes.append(
            ClassifiedBox(
                x1=0.20, x2=0.66, y1=0.18, y2=0.89, probability=0.95, name="organism"
            )
        )

    # ARISE output format
    media_id = os.path.splitext(os.path.split(uploaded_name)[-1])[0]
    out_json["media"].append({"filename": uploaded_name, "id": media_id})

    per_image_region_counter = 0

    for box in boxes:
        region_id = media_id + f"?region={per_image_region_counter}"

        out_json["region_groups"].append(
            {
                "id": region_id,
                # because individuals are not tracked (yet), we re-use the region_id as individual_id
                "individual_id": region_id,
                "regions": [
                    {
                        "box": {"x1": box.x1, "y1": box.y1, "x2": box.x2, "y2": box.y2},
                        "media_id": media_id,
                    }
                ],
            }
        )

        out_json["predictions"].append(
            {
                "region_group_id": region_id,
                "taxa": {
                    "type": "multiclass",
                    "items": [
                        {
                            "scientific_name": box.name,
                            "scientific_name_id": f"LITERAL:{box.name.upper()}",
                            "probability": np.round(box.probability, 6),
                        }
                    ],
                },
            }
        )

        per_image_region_counter += 1

    return flask.jsonify(out_json)


if __name__ == "__main__":
    app.run(
        debug=str2bool(os.getenv("DEBUG")),
        host=os.getenv("HOST", "0.0.0.0"),
        port=int(os.getenv("PORT", "5000")),
        use_reloader=False,
    )
